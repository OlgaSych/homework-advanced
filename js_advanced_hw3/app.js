"use strict";
class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get employeeName() {
        return `${this.name}`;
    }

    set employeeName(value) {
        this.name = value;
    }

    get employeeAge() {
        return `${this.age}`;
    }

    set employeeAge(value) {
        this.age = value;
    }

    get employeeSalary() {
        return `${this.salary}`;
    }

    set employeeSalary(value) {
        this.salary = value;
    }
}
class Programmer  extends Employee {
    constructor(name, age, salary, lang) {
        super(name,age,salary);
        this.lang = lang;
    };
    get programmerSalary () {
        super.employeeSalary;
        return (this.salary *3)
    };
};

const programmer1 =  new Programmer('Olga', '28', '5000', 'RU');
const programmer2 =  new Programmer('Max', '30', '2000', 'UA');
const programmer3 =  new Programmer('Rick', '22', '1800', 'EN');
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// JavaScript поддерживает наследование, что позволяет нам при создании новых типов объектов при необходимости 
// унаследовать их функционал от уже существующих.Например, у нас может быть объект User, представляющий отдельного // пользователя.И также может быть объект Employee, который представляет работника.Но работник также может являться // пользователем и поэтому должен иметь все его свойства и методы.