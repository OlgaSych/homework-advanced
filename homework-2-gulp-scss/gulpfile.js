const { src, dest, parallel, series, watch } = require("gulp");
const sass = require('gulp-sass');
const autoPrefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const jsMinify = require('gulp-js-minify');
const imageMin = require('gulp-imagemin');
const uglify = require('gulp-uglify');

                                                     //рабочее задание build

const cleanDist = () => {          //очистка папки dist
    return src('./dist', { read: false }).pipe(clean());
};

const sassModify = () => {        
    return src('./src/scss/*.scss')
        .pipe(sass())          //компиляция scss файлов в css
        .pipe(autoPrefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], { cascade: true }))          //добавление вендорных префиксов к CSS свойствам
        .pipe(concat("styles.min.css"))          //конкатенация всех css файлов 
        .pipe(cleanCss())          //минификация css
        .pipe(dest("./dist/"))          //копирование минифицированного styles.min.css в папку dist
        .pipe(browserSync.reload({
             stream: true
             }))
};

const scripts = () => {
    return src('./src/js/*.js')
        .pipe(concat("scripts.min.js"))          ////конкатенация всех js файлов 
        .pipe(jsMinify())          ////минификация js
        .pipe(dest("./dist/"))          //копирование минифицированного scripts.min.css в папку dist
        .pipe(browserSync.reload({
            stream: true
        }))
};

const images = () => {
    return src('./src/img/**/*.png')
        .pipe(imageMin())          //оптимизация картинок 
        .pipe(dest("./dist/img/"))
        .pipe(browserSync.reload({
            stream: true
        }))
};

                                                     //Рабочее задание dev

const serve = () => {      //запуск сервера и последующее отслеживание изменений *.js и *.scss файлов в папке src
    browserSync.init({
        server: {
            baseDir: "."
        },
        notify: false
    });
};

const watcher = () => {          //при изменении - пересборка и копирование объединенных и минифицированных файлов styles.min.css и scripts.min.js в папку dist, перезагрузка html-страницы
    watch("./src/scss/*.scss", (cb) => {
		browserSync.reload();
		cb();
    });        
     watch("./src/js/*.js", (cb) => {
		browserSync.reload();
		cb();
	});        
    watch("./index.html").on('change', browserSync.reload);
};

exports.dev = parallel(serve, watcher, series(sassModify, scripts, images));
exports.build = series(cleanDist, sassModify, scripts, images);